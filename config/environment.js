const development = {
    name: 'development',
    asset_path: './assets',
    session_cookie_key: 'blahsomething',
    db: 'codeial_development',
    smtp: {
        service: 'gmail',
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        auth: {
            user: 'noreply.codeial',
            pass: 'z0Th@n345'
        }
    },
    google_client_id: "508099628959-mh97p1n3qjrv793p2mdh2sr28toiqe31.apps.googleusercontent.com",
    goole_client_secret: "2ENU4PgBADIYZIRIxosLx2QE",
    goole_call_back_url: "http://localhost:8000/users/auth/google/callback",
    jwt_secret: 'codeial'
}

const production = {
    name: 'production'
}


module.exports = development;